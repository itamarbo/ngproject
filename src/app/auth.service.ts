import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from './interfaces/user';
import { AngularFireAuth } from '@angular/fire/auth';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: Observable<User | null>

  constructor(public afAuth:AngularFireAuth) {
    this.user = this.afAuth.authState;
   }

   signup(email:string , password:string){
    this.afAuth
      .auth
      .createUserWithEmailAndPassword(email,password)
      .then(res => console.log('Wiwi', res))
      
  }

  logout(){
    this.afAuth.auth.signOut().then(res => console.log('Yahuuu you logout', res));
  }

  login(email:string, password:string){
    this.afAuth.auth.signInWithEmailAndPassword(email,password)
    .then(res => console.log('Yahuuu you login', res))

  }

}
