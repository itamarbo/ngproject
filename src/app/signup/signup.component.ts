import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  email: string;
  password: string;

  constructor(public authService:AuthService, public router:Router) { }

  onSubmit(){
    this.authService.signup(this.email, this.password);
    this.router.navigate(['/welcome']);
  }

  ngOnInit() {
  }

}
