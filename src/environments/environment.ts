// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAxhOGKwRvhv-9YHHYzQl4LMKJ4HbKG6qI",
    authDomain: "ngproject-293b2.firebaseapp.com",
    databaseURL: "https://ngproject-293b2.firebaseio.com",
    projectId: "ngproject-293b2",
    storageBucket: "ngproject-293b2.appspot.com",
    messagingSenderId: "526463877931",
    appId: "1:526463877931:web:79335b52c679455f1a7b6e",
    measurementId: "G-39KSEDH7MP"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
